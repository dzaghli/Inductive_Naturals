{ mkDerivation, base, stdenv }:
mkDerivation {
  pname = "NaturalStuff";
  version = "0.1.0.0";
  src = ./.;
  libraryHaskellDepends = [ base ];
  description = "Natural numbers through induction";
  license = stdenv.lib.licenses.bsd3;
}

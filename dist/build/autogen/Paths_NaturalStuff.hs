{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_NaturalStuff (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/admin/.cabal/bin"
libdir     = "/Users/admin/.cabal/lib/x86_64-osx-ghc-8.0.2/NaturalStuff-0.1.0.0-lRtmqndJ12L9HSYyJGUFP"
dynlibdir  = "/Users/admin/.cabal/lib/x86_64-osx-ghc-8.0.2"
datadir    = "/Users/admin/.cabal/share/x86_64-osx-ghc-8.0.2/NaturalStuff-0.1.0.0"
libexecdir = "/Users/admin/.cabal/libexec"
sysconfdir = "/Users/admin/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "NaturalStuff_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "NaturalStuff_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "NaturalStuff_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "NaturalStuff_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "NaturalStuff_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "NaturalStuff_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)

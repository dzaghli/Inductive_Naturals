module Natural where

import Control.Exception

data Nat = 
    O |
    S Nat
  deriving (Eq, Ord)

--- Show Instance ---

instance Show Nat where
    show x = show $ fromEnum x

--- ENUM INSTANCE ---

instance Enum Nat where 
    succ x = (S x)
    pred O = undefined
    pred (S x) = x
    toEnum x
        | x == 0 = O
        | otherwise =  succ $ toEnum (x-1)

    fromEnum x 
        | x == O = 0
        | otherwise = 1 + fromEnum (pred(x))

--- NUM INSTANCE ---

-- Addition
(∆) :: Nat -> Nat -> Nat
(∆) x y 
    | y == O = x
    | x == O = y
    | otherwise = succ $ x ∆ pred(y)

-- Multiplication
(¬) :: Nat -> Nat -> Nat
(¬) x y 
    | x == O || y == O = O
    | x == (S O) = x
    | y == (S O) = x
    | otherwise = x ∆ (x ¬ (pred y))

-- Substraction but sign is not that '-'
(–) :: Nat -> Nat -> Nat
(–) x y 
    | y == O = x
    | (x == O) && (y > O) = undefined
    | otherwise = x – pred(y)

instance Num Nat where 
    (+) x y = x ∆ y
    (*) x y = x ¬ y 
    (-) x y = x – y
    abs x = x
    signum x
        | x == O = 0
        | otherwise = 1
    fromInteger x
        | x == 0 = O
        | otherwise = S $ fromInteger (x-1) 

--χαςκελλ_∆ΗΟ